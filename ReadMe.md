# Connection and flashing of a programmable camera 
Here is a short description how to upload code to a camera

## Hardware
- ESP32-CAM-MB USB 
- 2micro USB ware
- microSD card (optional)

## Software
- Arduino IDE
- ESP32 add-on

## You may also need
- [Camera case STL files for 3D printing](https://github.com/circuitrocks/ESP32-RTSP/tree/master/3D)

## Instruction
 Install Arduino IDE 
 > - [x] Install ESP32 add-on
 > - [x] Board -> AI-Thinker ESP32-CAM
 > - [x] Tools -> Port and select the COM port the camera is connected to.
 
**Attention!** If the button "Port" is blocked or you can not find a port available then probably you should install drivers on your computer. Just search "CH340C drivers installation guide" in Google.
 > - [x] Open files *CameraWebServer.ino, camera_pins.h, camera_index.h, app_httpd.cpp*
 > - [x] Connect the camera
 > - [x] Insert your network credentials 
 >*ssid = "REPLACE_WITH_YOUR_SSID";* *password = "REPLACE_WITH_YOUR_PASSWORD";* 
 > - [x] Push the Upload button

## Other projects using ESP-32 CAM /Arduino


[Live stream from anywhere in the world over Internet](https://github.com/techiesms/esp32-cam-mjpeg)

[Face Recognition Open Door Lock](https://github.com/robotzero1/esp32cam-access-control)

[Fire Recognition & Extinguisher system using Image processing and Arduino](https://youtu.be/Ix416lAYRSg)

